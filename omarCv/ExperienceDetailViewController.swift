//
//  ExpierenceDetailViewController.swift
//  omarCv
//
//  Created by Omar Zidan on 2019-11-03.
//  Copyright © 2019 OmarZidan. All rights reserved.
//

import UIKit

class ExperienceDetailViewController: UIViewController {

    
    
    @IBOutlet weak var imageOfDetailExp: UIImageView!
    @IBOutlet weak var titleOfDetailExp: UILabel!
    @IBOutlet weak var dateOfDetailExp: UILabel!
    @IBOutlet weak var descriptionOfDetailExp: UILabel!
    
    var recievedImageFromCell: UIImage?
    var recievedTitleFromCell: String = ""
    var recievedDateFromCell: String = ""
    var recievedDescriptionFromCell: String = ""
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = recievedTitleFromCell
        self.imageOfDetailExp.image = recievedImageFromCell
        self.titleOfDetailExp.text = self.recievedTitleFromCell
        self.dateOfDetailExp.text = self.recievedDateFromCell
        self.descriptionOfDetailExp.text = self.recievedDescriptionFromCell
    }
    

}

