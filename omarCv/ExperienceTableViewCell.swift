//
//  ExperienceTableViewCell.swift
//  omarCv
//
//  Created by Omar Zidan on 2019-11-07.
//  Copyright © 2019 OmarZidan. All rights reserved.
//

import UIKit

class ExperienceTableViewCell: UITableViewCell {

    
    @IBOutlet weak var imageOfCell: UIImageView!
    @IBOutlet weak var titleOfCell: UILabel!
    @IBOutlet weak var dateOfCell: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
