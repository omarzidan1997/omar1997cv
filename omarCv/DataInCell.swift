//
//  DataInCell.swift
//  omarCv
//
//  Created by Omar Zidan on 2019-11-04.
//  Copyright © 2019 OmarZidan. All rights reserved.
//

import UIKit
import Foundation

class DataInCell {
    
    private var title: String
    private var date: String
    private var image: UIImage?
    
    
    init(title: String, date: String, image: UIImage?) {
        self.title = title
        self.date = date
        if image != nil{
            self.image = image
        }
    }
    
    func getTitle() -> String {
        return self.title
    }
    func getDate() -> String {
        return self.date
    }
    func getImage() -> UIImage? {
        return self.image
    }
    func getDescription() -> String {
        switch self.title {
        case "Arla":
            return """
            The majority of the feed used by Arla farmers is produced by the farmers themselves. This requires less transport and reduces the amount of third party feeds that are imported. In turn, this reduces our carbon footprint. When our farmers produce their own cows’ feed, they know and control its quality to ensure sustainable, consistent, high quality milk production.
            """
        case "Huskvarna":
            return """
            Husqvarna Group is a global leading producer of outdoor power products for forest, park and garden care
            Our products include chainsaws, trimmers, robotic lawn mowers and ride-on lawn mowers. The Group is also the European leader in garden watering products and a global leader in cutting equipment and diamond tools for the construction and stone industries.

            The Group's products and solutions are sold under brands including Husqvarna, Gardena, McCulloch, Flymo, Zenoah and Diamant Boart via dealers and retailers to consumers and professionals in more than 100 countries. Net sales in 2018 amounted to SEK 41 billion, and the Group has more than 13,000 employees in 40 countries.
            """
        case "IES":
            return """
             I would like to introduce our schools, International English Schools, in this brief presentation. When starting The English School in 1993, it was with the conviction that commanding the English language is imperative in order for youngsters to be able to realize their full potential in the modern world. We can no longer comfort ourselves as educators that Swedish school children learn to speak English “well”. That is not enough. We have to be a step ahead and ensure they acquire a total inner security when speaking English, being able to explore and discuss issues in-depth in English.

            Our schools offer the best environment for achieving this. We recruit the main part of our teachers from English speaking countries. We provide an international, English speaking culture in our schools. A major part of our teaching is in English, beginning as early as grade 4. Our Upper Secondary school, “Internationella Engelska Gymnasiet” has both the Natural Science and the Social Science programs taught completely in English.
            """
        case "ED":
            return """
            From 1949 until today, ED's education and its organization have changed in different ways. The Technical High School started in 1949 and was a state educational institution for a three-year engineering degree. Initially, there were two classes and a total of 60 students, the school was staffed by a headmaster and 12 teachers as well as some single members of the other staff. During the 50s, the school expanded and the number of classes became 10 and the number of pupils and staff drop more than doubled. Many of the students were “adults” and had work experience before they started studying.
            """
         
        default:
            return "No text yet!"
        }
    }
}
