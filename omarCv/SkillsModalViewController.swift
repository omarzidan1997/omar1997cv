//
//  SkillsModalViewController.swift
//  omarCv
//
//  Created by Omar Zidan on 2019-11-05.
//  Copyright © 2019 OmarZidan. All rights reserved.
//

import UIKit

class SkillsModalViewController: UIViewController{
    
    
    @IBOutlet weak var imageToAnimate: UIImageView!
    @IBOutlet weak var dissButton: UIButton!
    
    var screenWidth  = UIScreen.main.bounds.width
    var screenHeight = UIScreen.main.bounds.height
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        self.self.preferredContentSize = CGSize(width: 100, height: 100)
        self.imageToAnimate.frame = CGRect(x: 0, y: 0, width: 100.0, height: 100.0)
        self.imageToAnimate.center = CGPoint(x:self.screenWidth / 2, y: screenHeight / 2)
        self.dissButton.alpha = 0.0
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        UIView.animate(withDuration: 0.5, animations: {
            self.imageToAnimate.frame = CGRect(x: 0, y: 0, width: self.screenWidth, height: self.screenHeight)
        }, completion: { (succeed) in
            if succeed{
                UIView.animate(withDuration: 0.5, animations: {
                    self.dissButton.alpha = 1
                })
            }
        })
    }
    
    
    @IBAction func dismissClicked(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
