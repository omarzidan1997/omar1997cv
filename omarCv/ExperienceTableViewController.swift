//
//  ExperienceTableViewController.swift
//  omarCv
//
//  Created by Omar Zidan on 2019-11-01.
//  Copyright © 2019 OmarZidan. All rights reserved.
//

import UIKit

class ExperienceTableViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

    }

   // MARK: - Table view data source
    let sectionsInTable = ["Work","Education"]
    var cellInfo = [
        [
          DataInCell(title:"Arla", date: "2015-2016", image: UIImage.self(named:"arla")),
          DataInCell(title:"Huskvarna", date: "2016-2018", image: UIImage.self(named:"huskvarna"))
        ],
        [
          DataInCell(title:"IES", date: "2010-2013",image: UIImage.self(named: "open-book")),
          DataInCell(title: "ED", date: "2013-2016",image: UIImage.self(named: "open-book"))
        ]

    ]
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return self.sectionsInTable.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellInfo[section].count
    }
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.sectionsInTable[section]
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "Experience", for:indexPath) as? ExperienceTableViewCell{
            cell.imageOfCell?.image = self.cellInfo[indexPath.section][indexPath.row].getImage();
            cell.titleOfCell.text = self.cellInfo[indexPath.section][indexPath.row].getTitle();
            cell.dateOfCell.text = self.cellInfo[indexPath.section][indexPath.row].getDate();
            return cell;
        }
        
        return UITableViewCell();
        
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let informationForCell: DataInCell? = cellInfo[indexPath.section][indexPath.row]
        performSegue(withIdentifier: "segueToExpDetailViewC", sender: informationForCell)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destToExpDetailView = segue.destination as? ExperienceDetailViewController
        let dataToSend: DataInCell? = sender as? DataInCell
        destToExpDetailView?.recievedTitleFromCell = dataToSend?.getTitle() ?? "couldnt get title"
        destToExpDetailView?.recievedDateFromCell = dataToSend?.getDate() ?? "couldnt get title"
        destToExpDetailView?.recievedDescriptionFromCell = dataToSend?.getDescription() ?? "couldnt get title"
        destToExpDetailView?.recievedImageFromCell = dataToSend?.getImage()
        
    }
}
